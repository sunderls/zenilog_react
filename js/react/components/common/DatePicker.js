var DatePicker = React.createClass({displayName: "DatePicker",
    mixins: [mixinZLPopup],
    reset: function(){

    },

    render: function(){
        return React.createElement("div", {className: "zl-datepicker"}, 
                "datepicker", 
                React.createElement("button", {onClick: this.hide, className: "button-cancel"}, "cancel")
            )
    }
});
