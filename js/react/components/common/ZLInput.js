var ZLInput = React.createClass({displayName: "ZLInput",
    focus: function(){
        var $commonPopup = $('.zl-common-popup');
        React.render(
            React.createElement(DatePicker, null),
            $commonPopup[0]
        )
    },
    render: function(){
        return React.createElement("div", {className: "zl-input", onClick: this.focus}, this.props.children)
    }
});
