var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
var InoutList = React.createClass({displayName: "InoutList",
    render: function(){
        
        return (
            React.createElement("div", null, 
                React.createElement(ReactCSSTransitionGroup, {transitionName: "list-item"}, 
                    this.props.data.map(function(inout, i){
                        return React.createElement(InoutItem, {data: inout, key: inout.created_at})
                    })
                )
            )
        )
    }
});
