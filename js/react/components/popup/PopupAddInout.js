var PopupAddInout = React.createClass({displayName: "PopupAddInout",
    mixins: [popupComponentMixin],
    getInitialState: function(){
        return {
            amount: this.props.amount ? this.props.amount : ''
        }
    },

    confirm: function(){
        var amount = this.state.amount,
            date = React.findDOMNode(this.refs.inputDate).value;
        $(window).trigger('PopupAddInout.done', {
                amount: amount * 1,
                date: (new Date(date)).getTime(),
                freq: 'o',
                created_at: Date.now()
            });
    },

    onClickHandler: function(obj, value){
        var amount = this.state.amount + $(obj.nativeEvent.target).text();
        this.setState({
            amount: amount
        })
    },

    backspace: function(){
        var amount = this.state.amount;
        this.setState({
            amount: amount.slice(0,amount.length - 1)
        });
    },
    render: function(){
        var self = this;
        return (
            React.createElement("div", {className: "popup-addinout"}, 
            React.createElement("div", {className: "popup-addinout__input"}, 
            React.createElement("div", {className: "label"}, "¥"), 
            React.createElement("span", {className: "num"}, this.state.amount)
            ), 
            React.createElement(Numpad, {backspace: this.backspace, onClickHandler: this.onClickHandler}), 
            React.createElement(ZLInput, {type: "date", ref: "inputDate"}), 
            React.createElement("div", {className: "popup-addinout__actions"}, 
                React.createElement("button", {onClick: this.confirm, className: "button-confirm"}, "confirm"), 
                React.createElement("button", {onClick: this.hide, className: "button-cancel"}, "cancel")
            )
            )
        )
    }
});
