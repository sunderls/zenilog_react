var PopupDatePicker = React.createClass({displayName: "PopupDatePicker",
    mixins: [popupComponentMixin],
    componentDidMount: function(){
        var that = this;
        $(React.findDOMNode(this.refs.datePicker)).datepicker()
            .on('changeDate', function(e){
                that.hide();
                $(window).trigger('datechanged', e.date);
            })
    },
    render: function(){
        return React.createElement("div", {className: "popup-datepicker"}, 
            React.createElement("div", {className: "date-picker", ref: "datePicker"}), 
                React.createElement("button", {onClick: this.hide, className: "button-cancel"}, "cancel")
            )
    }
});
