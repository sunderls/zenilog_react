var InoutForm = React.createClass({displayName: "InoutForm",

    addNewInout: function(){
        var amount = React.findDOMNode(this.refs.amount).value.trim();
        this.props.onAddNewInout(amount * 1);
    },
    render: function(){
        return (
            React.createElement("div", null, 
                React.createElement("input", {type: "text", ref: "amount"}), 
                React.createElement("button", {onClick: this.addNewInout}, "Add")
            )
        )
    }
});
