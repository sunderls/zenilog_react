var DateHeader = React.createClass({displayName: "DateHeader",
    changeDate: function(){
        $(window).trigger('popup.pop', {popupName: 'popup-datepicker', popupExtra: {}});
    },

    showprev: function(){
        $(window).trigger('showprevday');
    },

    shownext: function(){
        $(window).trigger('shownextday');
    },

    render: function(){
        return (
            React.createElement("div", {className: "date-header"}, 
                React.createElement("button", {className: "date-header-left", onClick: this.showprev}), 
                React.createElement("button", {className: "date-header-right", onClick: this.shownext}), 
                React.createElement("h1", {className: "date-header-center", onClick: this.changeDate}, Helper.formatDate(this.props.date, 'YYYY / MM / DD'))
            )
        )
    }
});

