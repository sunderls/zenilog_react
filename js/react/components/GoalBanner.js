var GoalBanner = React.createClass({displayName: "GoalBanner",
    getInitialState: function(){
        return {
            state: 0
        }
    },
    jumpState: function(){
        var state = (this.state.state + 1) % 2;
        this.setState({state: state});
    },
    render: function(){

        var now = new Date(),
            goal = this.props.goal;

        if (!goal){
            return React.createElement("div", null)
        }
        var endDay = new Date(goal.date);


        var rest = {
            //once
            o: 1,
            d: Math.floor((endDay - new Date()) / (1000 * 3600 * 24)),
            m: endDay.getMonth() - now.getMonth() + 1
        };

        rest.w = Math.floor(rest.d / 7);

        var totalExpense = this.props.totalExpense,
            totalBalance = this.props.totalBalance,
            totalIncome = this.props.totalIncome;

        var disposableEveryDay = (totalBalance + totalIncome + totalExpense - goal.amount) / rest.d;

        return (
            React.createElement("div", {className: "goal-banner state-" + this.state.state, onClick: this.jumpState}, 
                React.createElement("p", {className: "goal-banner__header show-at-state-0"}, 
                   React.createElement("span", {className: "rest-date"}, rest.d, " days"), 
                   React.createElement("span", {className: "title"}, "GOAL ", goal.amount, " @ ", goal.date)
                ), 
                React.createElement("p", {className: "goal-banner__header show-at-state-1"}, 
                   React.createElement("span", {className: "rest-date"}, rest.d, " days"), 
                   React.createElement("span", {className: "title"}, "@ ", goal.date)
                ), 

                React.createElement("div", {className: "goal-banner__body show-at-state-1"}, 
                    React.createElement("div", {className: "block block-goal"}, 
                        React.createElement("div", null, 
                        React.createElement("div", {className: "num"}, goal.amount), 
                        React.createElement("div", {className: "label"}, "goal")
                        )
                    ), 

                    React.createElement("div", {className: "block block-mark"}, 
                        React.createElement("p", {className: "mark mark-equal"}, "=")
                    ), 

                    React.createElement("div", {className: "block block-income"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, totalBalance + totalIncome), 
                        React.createElement("p", {className: "label"}, "income")
                        )
                    ), 

                    React.createElement("div", {className: "block block-mark"}, 
                        React.createElement("p", {className: "mark mark-equal"}, "-")
                    ), 

                    React.createElement("div", {className: "block block-expense"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, totalExpense), 
                        React.createElement("p", {className: "label"}, "expense")
                        )
                    ), 

                    React.createElement("div", {className: "block block-mark"}, 
                        React.createElement("p", {className: "mark mark-minus"}, "-")
                    ), 

                    React.createElement("div", {className: "block block-today"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, disposableEveryDay.toFixed(3)), 
                        React.createElement("p", {className: "label"}, "quota today")
                        )
                    )
                ), 

                React.createElement("div", {className: "goal-banner__body show-at-state-0"}, 
                    React.createElement("div", {className: "block block-today block-today-center"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, disposableEveryDay.toFixed(3)), 
                        React.createElement("p", {className: "label"}, "you can spend today")
                        )
                    )
                )
            )
        )
    }
});

