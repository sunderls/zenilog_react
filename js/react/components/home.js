// homepage
var Home = React.createClass({displayName: "Home",
    resetData: function(){
        $(window).trigger('data.reset');
    },

    filteredInouts: function(){
        var inouts = this.props.data.inouts;
        window.displayDate = this.props.display.date;
        var date = Helper.formatDate(this.props.display.date, 'YYYY-MM-DD');

        var dateStamp = new Date(date).getTime();
        window.dateStamp = dateStamp;
        window.inouts = inouts;
        var result = inouts.filter(function(el){
            return el.date >= dateStamp && el.date < dateStamp + 24 * 3600 * 1000
        });
        return result;
    },

    render: function(){

        var data = this.props.data,
            inouts = [];
        console.log(data);
        inouts = this.filteredInouts();

        var totalIncome = 0, totalExpense = 0, totalBalance = 0;


        for (var i = 0, total = inouts.length; i < total; i++){
            var item = inouts[i];
            if (item.freq === 1){
                totalIncome += item.amount;
            }
        }

        for (var i = 0, total = data.balances.length; i < total; i++){
            totalBalance += data.balances[i].amount;
        }

        var goal = data.goals ? data.goals[0] : null;

        return (
            React.createElement("div", {className: "home"}, 
                React.createElement(DateHeader, {date: this.props.display.date}), 
                React.createElement(GoalBanner, {totalBalance: totalBalance, totalIncome: totalIncome, totalExpense: totalExpense, goal: goal}), 
                React.createElement(InoutList, {data: inouts}), 
                React.createElement("div", {className: "bottom-bar"}, 
                    React.createElement(TriggerAddInout, {type: "in"}), React.createElement(TriggerAddInout, {type: "out"}), "  ", React.createElement("button", {onClick: this.resetData}, "reset Data")
                )
            )
        );
    }
});
