var TriggerAddInout = React.createClass({displayName: "TriggerAddInout",
    mixins: [TriggerPopup],
    popupName: 'PopupAddInout',
    componentDidMount: function(){
        this.popupExtra = {
            amount: this.props.type == 'in' ? '+' : '-'
        }
    },
    render: function(){
        var className = 'button-add-inout button-add-inout-' + this.props.type;

        return (
            React.createElement("button", React.__spread({onClick: this.popup},  this.props, {className: className}), this.props.children)
        )
    }
});

