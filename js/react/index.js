// homepage
var App = React.createClass({displayName: "App",
    getInitialState: function(){
        var that = this;
        Data.get().then(function(store){
            that.setState({data: store})
        });

        return {
            data: {
                inouts: [],
                balances: [],
                goals: []
            },
            route: '/home',
            // popup: false,
            // isShowingPopup: false,
            popup: 'PopupAddInout',
            isShowingPopup: true,

            display: {
                date: new Date(),
            }
        }
    },

    componentDidMount: function(){
        var that = this;
        $(window).on('popup.pop', function(e,data){
            console.log(data);
            that.setState({popup: data.popupName, popupExtra: data.popupExtra,isShowingPopup: true})
        });

        $(window).on('PopupAddInout.done', function(e, data){
            console.log('done input', data);
            var newInout = $.extend({}, data);
            that.state.data.inouts = that.state.data.inouts.concat([newInout]);
            that.setState({
                data: that.state.data,
                isShowingPopup: false
            })

            Data.sync();
        });

        $(window).on('inout.remove', function(e,data){
            var inouts = that.state.data.inouts,
                item = data;

            inouts.splice(inouts.indexOf(item), 1);
            console.log(inouts);
            that.setState({
                data: that.state.data
            })

            Data.sync();
        })

        $(window).on('showprevday', function(){
            var date = that.state.display.date;
            var newDate = new Date(date.getTime() - 24 * 3600 * 1000);
            that.state.display.date = newDate;
            that.setState({display: that.state.display});
        });

        $(window).on('shownextday', function(){
            var date = that.state.display.date;
            var newDate = new Date(date.getTime() + 24 * 3600 * 1000);
            that.state.display.date = newDate;
            that.setState({display: that.state.display});
        });

        $(window).on('popup.hide', function(e, data){
            that.hidePopup();
        });

        $(window).on('datechanged', function(e,date){
            console.log('datechanged', date);
            var display = that.state.display;
            display.date = date;
            that.setState({display: display});
        });

        $(window).on('data.reset', function(e,data){
            Data.mock();
            location.reload();
        });

    },
    hidePopup: function(){
        this.setState({isShowingPopup: false, popup: null});
    },

    render: function(){
        var page = null,
            popup = null;

        if (this.state.route === '/home'){
            page = React.createElement(Home, {data: this.state.data, display: this.state.display});
        } else {
            page = React.createElement("p", null, "404");
        }

        if (this.state.isShowingPopup){
            var popupContent = null;
            console.log(this.state.popupExtra);
            if (this.state.popup === 'PopupAddInout'){
                popupContent = React.createElement(PopupAddInout, React.__spread({},  this.state.popupExtra));
            } else if (this.state.popup === 'popup-datepicker'){
                popupContent = React.createElement(PopupDatePicker, null);
            }

            popup = React.createElement("div", {className: "popup"}, 
                popupContent
                )
        }

        return (
            React.createElement("div", {className: "wrapper"}, 
                page, 
                popup, 
                React.createElement("div", {className: "zl-common-popup"})
            )
        );
    }
});


React.render(
    React.createElement(App, null),
    $('#view')[0]
)
