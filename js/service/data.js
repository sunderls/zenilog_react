var Data = (function(){
    var api = {
        get: 'http://zenilog.colla.me/ken/get',
        set: 'http://zenilog.colla.me/ken/add'
    };

    var store = {};

    var key = Helper.getUrlParam('key');

    return {
        get: function(){
            var deferred = $.Deferred();

            $.get(api.get, {key: key}).then(function(result){
                // server data may be invalid
                // judge the last_updated time
                try {
                    var raw = JSON.parse(result);
                    if (raw.last_updated){
                        store = raw
                    }
                } catch (e) {
                    store = {};
                }

                finally {

                    if (!store.inouts){
                        store.inouts = [];
                    }

                    if (!store.balances){
                        store.balances = [];
                    }

                    if (!store.goals){
                        store.goals = [
                            {
                                date: '2015-12-31',
                                amount: 2400000
                            }
                        ];
                    }
                    deferred.resolve(store);
                }

            })

            return deferred;
        },
        // inout
        // {amount: , freq: 'o', date: xxx, created_at:'', updated_at:''}
        addInout: function(inout){
            if (!store.inouts){
                store.inouts = [];
            }

            store.inouts.push(inout);

            this.sync();
        },

        getInoutOfDay: function(dateStartStamp){
            return store.inouts.filter(function(el, index, arr){
                return el.date >= dateStartStamp && el.date <= dateStartStamp + 24 * 3600 * 1000
            });
        },

        addBalance: function(balance){
            if (!store.balances){
                store.balances = [];
            }

            store.balances.push(balance);
            this.sync();
        },


        // goal
        // {date: '2015-12-31', amount:240, created_at:2222}
        addGoal: function(goal){
            if (!store.goals){
                store.goals = [];
            }
            store.goals.push(goal);
            this.sync();
        },

        sync: function(){
            store.last_updated = Date.now();
            $.post(api.set, {key: key, value: JSON.stringify(store), csrf_token: Helper.getCSRF()});
        },

        clear: function(){
            return $.post(api.set, {key: key, value: 'null', csrf_token: Helper.getCSRF()});
        },

        mockData: {
            goals: [
                {
                    date: '2015-12-31',
                    amount: 2400000
                }
            ],
            balances: [],
            inouts: []
        },

        mock: function(){
            var data = this.mockData;
            data.last_updated = Date.now();
            $.get(api.set, {key: 'sunderls', value: JSON.stringify(data)});
        },
        getStore: function(){
            return store;
        }
    }
})();
