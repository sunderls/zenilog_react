var Helper = {
    formatDate: function(date, format){
        // temporily support only 'YYYY MM DD'
        var year = date.getFullYear(),
            month = date.getMonth() + 1,
            date = date.getDate();

        if (month < 10){ month = '0' + month; }
        if (date < 10){ date = '0' + date; }

        return format.replace('YYYY', year)
            .replace('MM', month)
            .replace('DD', date);
    },

    getDateString: (function(){
        var list = [ '', '1st', '2nd', '3rd'];

        for(var i = 4; i < 32;i++){
            list.push(i);
        }

        return function(i){
            return isNaN(i) ? i : list[i];
        }
    })(),

    getMonthString: (function(){
        var list = [ '', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return function(i){
            return isNaN(i) ? i : list[i];
        }
    })(),

    disableTouchmove: function(isDisable){
        $(window).off('touchmove.disable');

        if (isDisable){
            $(window).on('touchmove.disable', function(e){
                e.preventDefault();
            });
        }
    },

    getValueLabel: function(value, type){
        // 2015/5/24 => May 24, 2015 => o
        // 2015/5/every => everyday @ May, 2015 => d
        // 2015/5/some => May, 2015 => o

        // 2015/every/24 => 24th @every month, 2015 => m
        // 2015/every/every => every day, 2015 => d
        // 2015/every/some => every month, 2015 => m, X?

        // 2015/some/24 => X 
        // 2015/some/every => X
        // 2015/some/some => 2015 =>o
        return value;
        if (type == 'inout-date'){
            return value;
        }
    },

    getFreqOfDate: function(date){
        // possibilities are up there
        if (/\d+\/\d+\/every/.test(date)){
            return 'd';
        } else if (/\d+\/every\//.test(date)){
            return 'm';
        } else {
            return 'o';
        }
    }, 

    getUrlParam: (function(){
        var search = window.location.search;
        search = search.slice(1, search.length);

        var segs = search.split('&'),
            result = {};

        for (var i = 0, total = segs.length; i < total; i++){
            var seg = segs[i];
            var pairs = seg.split('=');
            result[pairs[0]] = pairs[1];
        }

        return function(key){
            return result[key];
        }
    })(),

    getDateDiff: function(start, end){
        var diff = {
             //once
            o: 1,
            d: Math.floor((end - start) / (1000 * 3600 * 24)),
            m: end.getMonth() - start.getMonth() + 1 //once
        };

        diff.w = Math.floor(diff.d / 7);

        return diff;
    },

    getCSRF: function(){
        var cookieSegs = document.cookie.split(';');
        for(var i = 0, total = cookieSegs.length; i < total;i++){
            var match = cookieSegs[i].match(/csrf_token=(.*)/);
            if (match){
                return match[1]
            }
        }
    }

}
