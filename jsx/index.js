// homepage
var App = React.createClass({
    getInitialState: function(){
        var that = this;
        Data.get().then(function(store){
            that.setState({data: store})
        });

        return {
            data: {
                inouts: [],
                balances: [],
                goals: []
            },
            route: '/home',
            popup: false,
            isShowingPopup: false,
            // popup: 'inout-editor',
            // isShowingPopup: true,

            display: {
                date: new Date(),
            }
        }
    },

    componentDidMount: function(){
        var that = this;
        $(window).on('popup.pop', function(e,data){
            console.log('pop',data);
            that.setState({popup: data.popupName, popupExtra: data.popupExtra,isShowingPopup: true})
        });

        $(window).on('PopupAddInout.done', function(e, data){
            var inouts = that.state.data.inouts,
                is_new = data.is_new;

            delete data.is_new;

            // 2015/5/24 => May 24, 2015 => o
            // 2015/5/every => everyday @ May, 2015 => d
            // 2015/5/some => May, 2015 => o

            // 2015/every/24 => 24th @every month, 2015 => m
            // 2015/every/every => every day, 2015 => d
            // 2015/every/some => every month, 2015 => m

            // 2015/some/24 => X
            // 2015/some/every => X
            // 2015/some/some => some time @2015 =>o
            data.freq = Helper.getFreqOfDate(data.date);
            console.log(data);
            if (is_new){
                var newInout = $.extend({}, data);
                inouts.push(newInout);
            } else {
                var updated = false;
                for(var i = 0, total = inouts.length; i < total; i++){
                    if (inouts[i].created_at == data.created_at){
                        $.extend(inouts[i], data);
                        updated = true;
                        break;
                    }
                }
                if (!updated){alert('not updated, inout item not found')}
            }

            that.setState({
                data: that.state.data,
                isShowingPopup: false
            })

            Data.sync();
        });

        $(window).on('PopupGoalEditor.done', function(e, data){
            console.log(data);
            var goal = that.state.data.goals.length ? that.state.data.goals[0] : {};

            $.extend(goal, data);

            that.setState({
                data:that.state.data,
                isShowingPopup: false
            })

            Data.sync();
        });

        $(window).on('inout.remove', function(e,data){
            var inouts = that.state.data.inouts,
                item = data;

            inouts.splice(inouts.indexOf(item), 1);
            that.setState({
                data: that.state.data
            })

            Data.sync();
        })

        $(window).on('showprevday', function(){
            var date = that.state.display.date;
            var newDate = new Date(date.getTime() - 24 * 3600 * 1000);
            that.state.display.date = newDate;
            that.setState({display: that.state.display});
        });

        $(window).on('shownextday', function(){
            var date = that.state.display.date;
            var newDate = new Date(date.getTime() + 24 * 3600 * 1000);
            that.state.display.date = newDate;
            that.setState({display: that.state.display});
        });

        $(window).on('popup.hide', function(e, data){
            that.hidePopup();
        });

        $(window).on('datechanged', function(e,date){
            console.log('datechanged', date);
            var display = that.state.display;
            display.date = date;
            that.setState({display: display});
        });

        $(window).on('data.reset', function(e,data){
            Data.mock();
            location.reload();
        });

    },
    hidePopup: function(){
        this.setState({isShowingPopup: false, popup: null});
    },

    render: function(){
        var page = null,
            popup = null;

        if (this.state.route === '/home'){
            page = <Home data={this.state.data} display={this.state.display}/>;
        } else {
            page = <p>404</p>;
        }

        if (this.state.isShowingPopup){
            var popupContent = null;
            if (this.state.popup === 'inout-editor'){
                popupContent = <PopupAddInout {...this.state.popupExtra}/>;
            } else if (this.state.popup === 'popup-datepicker'){
                popupContent = <PopupCalender {...this.state.popupExtra}/>;
            } else if (this.state.popup === 'goal-editor'){
                popupContent = <PopupGoalEditor {...this.state.popupExtra}/>
            }

            popup = <div className="popup">
                {popupContent}
                </div>
        }

        return (
            <div className='wrapper'>
                {page}
                {popup}
                <div className='zl-common-popup'></div>
            </div>
        );
    }
});

React.initializeTouchEvents(true);

React.render(
    <App />,
    $('#view')[0]
)

// React.render(
//     <Keyboard />,
//     $('.zl-common-popup')[0]
// )

$(window).on('touchmove', function(e){
    console.log(e.originalEvent.pageY);
});

