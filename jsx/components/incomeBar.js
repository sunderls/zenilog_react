var TriggerAddInout = React.createClass({
    mixins: [TriggerPopup],
    popupName: 'inout-editor',
    componentDidMount: function(){
        this.popupExtra = {
            amount: this.props.type == 'in' ? '+' : '-'
        }
    },
    render: function(){
        var className = 'button-add-inout button-add-inout-' + this.props.type;

        return (
            <button onClick={this.popup} {...this.props} className={className}>{this.props.children}</button>
        )
    }
});

