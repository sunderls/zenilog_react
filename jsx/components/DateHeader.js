var DateHeader = React.createClass({
    changeDate: function(){
        $(window).trigger('popup.pop', {popupName: 'popup-datepicker', popupExtra: {}});
    },

    showprev: function(){
        $(window).trigger('showprevday');
    },

    shownext: function(){
        $(window).trigger('shownextday');
    },

    render: function(){
        return (
            <div className="date-header">
                <button className="date-header-left" onClick={this.showprev}></button>
                <button className="date-header-right" onClick={this.shownext}></button>
                <h1 className="date-header-center" onClick={this.changeDate}>{Helper.formatDate(this.props.date, 'YYYY / MM / DD')}</h1>
            </div>
        )
    }
});

