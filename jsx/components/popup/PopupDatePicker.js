var PopupCalender = React.createClass({
    mixins: [popupComponentMixin],
    componentDidMount: function(){
        var that = this;
        $(React.findDOMNode(this.refs.datePicker)).datepicker()
            .on('changeDate', function(e){
                that.hide();
                $(window).trigger('datechanged', e.date);
            })
    },
    render: function(){
        return <div className="popup-datepicker">
                <div className="date-picker" ref="datePicker"></div>
                <div className="controls">
                    <button onClick={this.confirm} className="button-confirm">OK</button>
                    <button onClick={this.hide} className="button-cancel">cancel</button>
                </div>
            </div>
    }
});
