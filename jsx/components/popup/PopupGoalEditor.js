var PopupGoalEditor = React.createClass({
    mixins: [popupComponentMixin],
    getInitialState: function(){
        console.log('init', this.props.goal);
        return this.props.goal|| {amount:''};
    },

    confirm: function(){
        var amount = this.state.amount,
            date = this.refs.inputDate.state.value;

        $(window).trigger('PopupGoalEditor.done', {
            amount: amount * 1,
            date: new Date(date).getTime(),
            created_at: Date.now(),
        });
    },

    onClickHandler: function(obj, value){
        var amount = this.state.amount + $(obj.nativeEvent.target).text();
        this.setState({
            amount: amount
        })
    },

    backspace: function(){
        var amount = '' + this.state.amount;
        this.setState({
            amount: amount.slice(0,amount.length - 1)
        });
    },
    render: function(){
        var self = this;
        console.log('render', this.state.amount);
        return (
            <div className="popup-goal-editor">
            <h1 className="title">edit the goal</h1>
            <div className="input-amount">
            <div className="label">¥</div>
            <span className="num">{this.state.amount}</span>
            </div>
            <Numpad backspace={this.backspace} onClickHandler={this.onClickHandler} />
            <ZLInput type="date" ref="inputDate" placeholder="select date" value={this.state.date} label="before "/>
            <div className="actions">
                <button onClick={this.confirm} className="button-confirm">OK</button>
                <button onClick={this.hide} className="button-cancel">cancel</button>
            </div>
            </div>
        )
    }
});
