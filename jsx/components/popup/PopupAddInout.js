var PopupAddInout = React.createClass({
    mixins: [popupComponentMixin],
    getInitialState: function(){
        return this.props.inout || {amount: this.props.amount || ''}
    },

    confirm: function(){
        var amount = this.state.amount,
            memo = this.refs.memo.state.value,
            date = this.refs.inputDate.state.value,
            dateStamp = new Date(date).getTime();

        // if date is not valide, new Date() is not invalid.
        // but getTime will return NaN

        var newInout = {
            amount: amount * 1,
            date: date,
            freq: 'o',
            created_at: this.state.created_at || Date.now(),
            is_new: !this.state.created_at,
            memo: memo
        }

        if (dateStamp){
            newInout.dateStamp = dateStamp;
        }

        if (this.refs.startDate){
            newInout.startDate = this.refs.startDate.state.value;
        }

        if (this.refs.endDate){
            newInout.endDate = this.refs.endDate.state.value;
        }

        $(window).trigger('PopupAddInout.done', newInout);
    },

    onClickHandler: function(obj, value){
        var amount = '' + this.state.amount + $(obj.nativeEvent.target).text();
        this.setState({
            amount: amount
        })
    },

    backspace: function(){
        var amount = '' + this.state.amount;
        this.setState({
            amount: amount.slice(0,amount.length - 1)
        });
    },

    updateDate: function(date){
        console.log('PopupAddInout', date);
        this.setState({date: date});
    },

    render: function(){
        var self = this;
        var date = this.state.date,
            isEveryMonth = date && date.split('/')[1] == 'every';

        console.log('isEveryMonth', isEveryMonth);
        var period = false;
        if (isEveryMonth){
            period = <div>
                <ZLInput type="date" ref="startDate" placeholder="start from:" value={this.state.startDate} label="start from "/>
                <ZLInput type="date" ref="endDate" placeholder="end at" value={this.state.endDate} label="end @ "/>
                </div>
        }
        return (
            <div className="popup-addinout">
            <div className="popup-addinout__input">
            <div className="label">¥</div>
            <span className="num">{this.state.amount}</span>
            </div>
            <Numpad backspace={this.backspace} onClickHandler={this.onClickHandler} />
            <ZLInput type="inout-date" ref="inputDate" placeholder="select date" label="@" value={this.state.date} onChange={this.updateDate}/>
            {period}
            <ZLInput type="text" ref="memo" placeholder="memo..." value={this.state.memo}/>
            <div className="popup-addinout__actions">
                <button onClick={this.confirm} className="button-confirm">OK</button>
                <button onClick={this.hide} className="button-cancel">cancel</button>
            </div>
            </div>
        )
    }
});
