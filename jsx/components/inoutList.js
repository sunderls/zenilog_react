var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
var InoutList = React.createClass({
    componentDidMount: function(){
        var $dom = React.findDOMNode(this);
    },
    componentDidUpdate: function(){
        var $dom = React.findDOMNode(this);
            height = window.innerHeight - 123 - 70;

        $($dom).height(height);
        Helper.disableTouchmove(false);
        $(window).off('touchstart.disable');

        var touchY = 0,
            y = 0,
            scrollTop = 0;
        if (height < $dom.scrollHeight){
            $(window).on('touchstart.disable', function(e){
                touchY = e.originalEvent.pageY;
                if ($(e.target).parents().filter('.inout-list').size() > 0){
                    $(window).on('touchmove.disable', function(e){
                        y = e.originalEvent.pageY;
                        scrollTop = $($dom).scrollTop();

                        if ($($dom).scrollTop() == 0 && y > touchY){
                            e.preventDefault();
                        } else if (scrollTop + height >= $dom.scrollHeight && y < touchY){
                            e.preventDefault();
                        }
                    });

                    $(window).one('touchend', function(){
                        Helper.disableTouchmove(false);
                    });
                } else {
                    Helper.disableTouchmove('disable');
                }
            })
        } else {
            Helper.disableTouchmove('disable');
        }
        
    },
    render: function(){
        
        return (
            <div className="inout-list needslick">
                <div>
                <ReactCSSTransitionGroup transitionName="list-item">
                {this.props.data.map(function(inout, i){
                    return <InoutItem data={inout} key={inout.created_at}/>
                })}
                </ReactCSSTransitionGroup></div>
            </div>
        )
    }
});
