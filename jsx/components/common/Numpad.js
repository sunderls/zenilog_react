var Numpad = React.createClass({

    render: function(){
        return (
            <div className="numpad">
                <div value="1" onClick={this.props.onClickHandler} className="numpad-key numpad-key-1">1</div>
                <div value="2" onClick={this.props.onClickHandler} className="numpad-key numpad-key-2">2</div>
                <div value="3" onClick={this.props.onClickHandler} className="numpad-key numpad-key-3">3</div>
                <div value="4" onClick={this.props.onClickHandler} className="numpad-key numpad-key-4">4</div>
                <div value="5" onClick={this.props.onClickHandler} className="numpad-key numpad-key-5">5</div>
                <div value="6" onClick={this.props.onClickHandler} className="numpad-key numpad-key-6">6</div>
                <div value="7" onClick={this.props.onClickHandler} className="numpad-key numpad-key-7">7</div>
                <div value="8" onClick={this.props.onClickHandler} className="numpad-key numpad-key-8">8</div>
                <div value="9" onClick={this.props.onClickHandler} className="numpad-key numpad-key-9">9</div>
                <div value="0" onClick={this.props.onClickHandler} className="numpad-key numpad-key-0">0</div>
                <div value="." onClick={this.props.onClickHandler} className="numpad-key numpad-key-dot">.</div>
                <div onClick={this.props.backspace} className="numpad-key numpad-key-backspace"></div>
            </div>
        )
    }
});

