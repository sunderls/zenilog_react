var Keyboard = React.createClass({
    mixins: [mixinZLPopup],
    getInitialState: function(){
        return {
            value: this.props.value || ''
        }
    },
    click: function(letter){
        var value = this.state.value;
        value += letter.toLowerCase();
        this.setState({value: value});
    },

    backspace: function(){
        var value = this.state.value;
        value = value.slice(0, value.length - 1);
        this.setState({value: value});
    },

    confirm: function(){
        var value = this.state.value;
        $(window).trigger('input.done', {
            value: value.toLowerCase()
        });
        this.hide();
    },

    cancel: function(){
        $(window).trigger('input.done', {
            value: null
        });
        this.hide();
    },

    render: function(){
        var that = this;

        var keys = [];
        function renderKeys(keys){
            return keys.split('').map(function(letter){
                if (letter == '\n'){
                    return <br />
                } else if (letter == ' ') {
                    return <div className="key key-space" onTouchStart={that.click.bind(that, letter)}><div className="inner">space</div></div>
                } else if (letter == '<') {
                    return <div className="key key-backspace" onTouchStart={that.backspace}><div className="inner">←</div></div>
                } else {
                    return <div className={"key key-" + letter} onTouchStart={that.click.bind(that, letter)}><div className="inner">{letter}</div></div>
                }
            });
        }

        return (
            <div className="keyboard">
                <div className="value">{this.state.value}</div>
                <div className="panel">
                    <div className="row1">{renderKeys('0123456789')}</div>
                    <div className="row2">{renderKeys('QWERTYUIOP')}</div>
                    <div className="row3">{renderKeys('ASDFGHJKL')}</div>
                    <div className="row4">{renderKeys('ZXCVBNM')}</div>
                    <div className="row5">{renderKeys(' .<')}</div>
                </div>
                <div className="controls">
                    <button onClick={this.confirm} className="button-confirm">OK</button>
                    <button onClick={this.cancel} className="button-cancel">cancel</button>
                </div>
            </div>
        )
    }
});

