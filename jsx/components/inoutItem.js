var InoutItem = React.createClass({
    getInitialState: function(){
        return {
            state: 0
        }
    },
    jumpState: function(){
        var state = (this.state.state + 1) % 2;
        this.setState({state: state});
    },
    remove: function(){
        $(window).trigger('inout.remove', this.props.data);
    },
    edit: function(){
        var inout = this.props.data;
        $(window).trigger('popup.pop', {popupName: 'inout-editor', popupExtra:{inout:inout}});
    },
    render: function(){
        var item = this.props.data;
        if (!item){
        	return <div className="inout-item">no in&out record</div>
        }
        return (
            <div className={"inout-item state-" + this.state.state} onClick={this.jumpState}>
                <button onClick={this.edit} className="inout-item__edit">EDIT</button>
                {item.amount > 0 ? '+' : ''}{item.amount}
                <button onClick={this.remove} className="inout-item__remove">DELETE</button>
            </div>
        );
    }
});
