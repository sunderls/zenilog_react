var GoalBanner = React.createClass({
    getInitialState: function(){
        return {
            state: 0
        }
    },
    jumpState: function(){
        var state = (this.state.state + 1) % 2;
        this.setState({state: state});
    },
    editGoal: function(){
        var goal = this.props.goal;
        $(window).trigger('popup.pop', {popupName: 'goal-editor', popupExtra:{goal:goal}});
    },
    render: function(){

        var goal = this.props.goal,
            date = new Date(this.props.date);

        if (!goal){
            return <div></div>
        }
        var endDay = new Date(goal.date);


        var rest = Helper.getDateDiff(date, endDay);

        var totalIncome = 0, totalExpense = 0,
            inoutsToDate = this.props.inoutsToDate;

        for (var i = 0, total = inoutsToDate.length; i < total; i++){
            var item = inoutsToDate[i];
            if (item.freq == 'o'){
                if (item.amount < 0){
                    totalExpense += item.amount;
                } else {
                    totalIncome += item.amount;
                }
            } else if (item.freq == 'm'){
                // suppose it has right start date,
                // maybe we should set CheckPoint?
                var m = (Helper.getDateDiff(new Date(item.startDate), endDay)).m;
                if (item.amount < 0){
                    totalExpense += item.amount * m;
                } else {
                    totalIncome += item.amount * m;
                }
            }
        }

        var disposableEveryDay = (totalIncome + totalExpense - goal.amount) / rest.d;

        return (
            <div className={"goal-banner state-" + this.state.state} onClick={this.jumpState}>
                <p className="goal-banner__header show-at-state-0">
                   <span className="rest-date">{rest.d} days</span>
                   <span className="title">GOAL {goal.amount} @ {Helper.formatDate(new Date(goal.date), 'YYYY-MM-DD')}</span>
                </p>
                <p className="goal-banner__header show-at-state-1">
                   <span className="edit" onClick={this.editGoal}>EDIT</span>
                   <span className="title">@ {Helper.formatDate(new Date(goal.date), 'YYYY-MM-DD')}</span>
                </p>

                <div className="goal-banner__body show-at-state-1">
                    <div className="block block-goal">
                        <div>
                        <div className="num">{goal.amount}</div>
                        <div className="label">goal</div>
                        </div>
                    </div>

                    <div className="block block-mark">
                        <p className="mark mark-equal">=</p>
                    </div>

                    <div className="block block-income">
                        <div>
                        <p className="num">{totalIncome}</p>
                        <p className="label">income</p>
                        </div>
                    </div>

                    <div className="block block-mark">
                        <p className="mark mark-equal">-</p>
                    </div>

                    <div className="block block-expense">
                        <div>
                        <p className="num">{-totalExpense.toFixed(4)}</p>
                        <p className="label">expense</p>
                        </div>
                    </div>

                    <div className="block block-mark">
                        <p className="mark mark-minus">-</p>
                    </div>

                    <div className="block block-today">
                        <div>
                        <p className="num">{disposableEveryDay.toFixed(3)}</p>
                        <p className="label">quota today</p>
                        </div>
                    </div>
                </div>

                <div className="goal-banner__body show-at-state-0">
                    <div className="block block-today block-today-center">
                        <div>
                        <p className="num">{disposableEveryDay.toFixed(3)}</p>
                        <p className="label">you can spend</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

