var InoutForm = React.createClass({

    addNewInout: function(){
        var amount = React.findDOMNode(this.refs.amount).value.trim();
        this.props.onAddNewInout(amount * 1);
    },
    render: function(){
        return (
            <div>
                <input type="text" ref="amount" />
                <button onClick={this.addNewInout}>Add</button>
            </div>
        )
    }
});
