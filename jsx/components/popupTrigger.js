var PopupTrigger = React.createClass({
    handleClick: function() {
        var popupName = this.props.popupName;
        alert(popupName);
    },
    render: function() {
        return <button onClick={this.handleClick()}>{this.props.text}</button>;
    }
});
