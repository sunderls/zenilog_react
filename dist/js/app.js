var Helper = {
    formatDate: function(date, format){
        // temporily support only 'YYYY MM DD'
        var year = date.getFullYear(),
            month = date.getMonth() + 1,
            date = date.getDate();

        if (month < 10){ month = '0' + month; }
        if (date < 10){ date = '0' + date; }

        return format.replace('YYYY', year)
            .replace('MM', month)
            .replace('DD', date);
    },

    getDateString: (function(){
        var list = [ '', '1st', '2nd', '3rd'];

        for(var i = 4; i < 32;i++){
            list.push(i);
        }

        return function(i){
            return isNaN(i) ? i : list[i];
        }
    })(),

    getMonthString: (function(){
        var list = [ '', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return function(i){
            return isNaN(i) ? i : list[i];
        }
    })(),

    disableTouchmove: function(isDisable){
        $(window).off('touchmove.disable');

        if (isDisable){
            $(window).on('touchmove.disable', function(e){
                e.preventDefault();
            });
        }
    },

    getValueLabel: function(value, type){
        // 2015/5/24 => May 24, 2015 => o
        // 2015/5/every => everyday @ May, 2015 => d
        // 2015/5/some => May, 2015 => o

        // 2015/every/24 => 24th @every month, 2015 => m
        // 2015/every/every => every day, 2015 => d
        // 2015/every/some => every month, 2015 => m, X?

        // 2015/some/24 => X 
        // 2015/some/every => X
        // 2015/some/some => 2015 =>o
        return value;
        if (type == 'inout-date'){
            return value;
        }
    },

    getFreqOfDate: function(date){
        // possibilities are up there
        if (/\d+\/\d+\/every/.test(date)){
            return 'd';
        } else if (/\d+\/every\//.test(date)){
            return 'm';
        } else {
            return 'o';
        }
    }, 

    getUrlParam: (function(){
        var search = window.location.search;
        search = search.slice(1, search.length);

        var segs = search.split('&'),
            result = {};

        for (var i = 0, total = segs.length; i < total; i++){
            var seg = segs[i];
            var pairs = seg.split('=');
            result[pairs[0]] = pairs[1];
        }

        return function(key){
            return result[key];
        }
    })(),

    getDateDiff: function(start, end){
        var diff = {
             //once
            o: 1,
            d: Math.floor((end - start) / (1000 * 3600 * 24)),
            m: end.getMonth() - start.getMonth() + 1 //once
        };

        diff.w = Math.floor(diff.d / 7);

        return diff;
    },

    getCSRF: function(){
        var cookieSegs = document.cookie.split(';');
        for(var i = 0, total = cookieSegs.length; i < total;i++){
            var match = cookieSegs[i].match(/csrf_token=(.*)/);
            if (match){
                return match[1]
            }
        }
    }

}

var Data = (function(){
    var api = {
        get: 'http://zenilog.colla.me/ken/get',
        set: 'http://zenilog.colla.me/ken/add'
    };

    var store = {};

    var key = Helper.getUrlParam('key');

    return {
        get: function(){
            var deferred = $.Deferred();

            $.get(api.get, {key: key}).then(function(result){
                // server data may be invalid
                // judge the last_updated time
                try {
                    var raw = JSON.parse(result);
                    if (raw.last_updated){
                        store = raw
                    }
                } catch (e) {
                    store = {};
                }

                finally {

                    if (!store.inouts){
                        store.inouts = [];
                    }

                    if (!store.balances){
                        store.balances = [];
                    }

                    if (!store.goals){
                        store.goals = [
                            {
                                date: '2015-12-31',
                                amount: 2400000
                            }
                        ];
                    }
                    deferred.resolve(store);
                }

            })

            return deferred;
        },
        // inout
        // {amount: , freq: 'o', date: xxx, created_at:'', updated_at:''}
        addInout: function(inout){
            if (!store.inouts){
                store.inouts = [];
            }

            store.inouts.push(inout);

            this.sync();
        },

        getInoutOfDay: function(dateStartStamp){
            return store.inouts.filter(function(el, index, arr){
                return el.date >= dateStartStamp && el.date <= dateStartStamp + 24 * 3600 * 1000
            });
        },

        addBalance: function(balance){
            if (!store.balances){
                store.balances = [];
            }

            store.balances.push(balance);
            this.sync();
        },


        // goal
        // {date: '2015-12-31', amount:240, created_at:2222}
        addGoal: function(goal){
            if (!store.goals){
                store.goals = [];
            }
            store.goals.push(goal);
            this.sync();
        },

        sync: function(){
            store.last_updated = Date.now();
            $.post(api.set, {key: key, value: JSON.stringify(store), csrf_token: Helper.getCSRF()});
        },

        clear: function(){
            return $.post(api.set, {key: key, value: 'null', csrf_token: Helper.getCSRF()});
        },

        mockData: {
            goals: [
                {
                    date: '2015-12-31',
                    amount: 2400000
                }
            ],
            balances: [],
            inouts: []
        },

        mock: function(){
            var data = this.mockData;
            data.last_updated = Date.now();
            $.get(api.set, {key: 'sunderls', value: JSON.stringify(data)});
        },
        getStore: function(){
            return store;
        }
    }
})();

var TriggerPopup = {
    popup: function(){
        var popup = this.popupName,
        	extra = this.popupExtra;

        $(window).trigger('popup.pop', {popupName: popup, popupExtra: extra});
    }
};

var mixinZLPopup = {
    hide: function(){
        React.unmountComponentAtNode($('.zl-common-popup')[0]);
    }
};

var popupComponentMixin = {
    hide: function(){
        $(window).trigger('popup.hide');
    }
};

var DateHeader = React.createClass({displayName: "DateHeader",
    changeDate: function(){
        $(window).trigger('popup.pop', {popupName: 'popup-datepicker', popupExtra: {}});
    },

    showprev: function(){
        $(window).trigger('showprevday');
    },

    shownext: function(){
        $(window).trigger('shownextday');
    },

    render: function(){
        return (
            React.createElement("div", {className: "date-header"}, 
                React.createElement("button", {className: "date-header-left", onClick: this.showprev}), 
                React.createElement("button", {className: "date-header-right", onClick: this.shownext}), 
                React.createElement("h1", {className: "date-header-center", onClick: this.changeDate}, Helper.formatDate(this.props.date, 'YYYY / MM / DD'))
            )
        )
    }
});


var GoalBanner = React.createClass({displayName: "GoalBanner",
    getInitialState: function(){
        return {
            state: 0
        }
    },
    jumpState: function(){
        var state = (this.state.state + 1) % 2;
        this.setState({state: state});
    },
    editGoal: function(){
        var goal = this.props.goal;
        $(window).trigger('popup.pop', {popupName: 'goal-editor', popupExtra:{goal:goal}});
    },
    render: function(){

        var goal = this.props.goal,
            date = new Date(this.props.date);

        if (!goal){
            return React.createElement("div", null)
        }
        var endDay = new Date(goal.date);


        var rest = Helper.getDateDiff(date, endDay);

        var totalIncome = 0, totalExpense = 0,
            inoutsToDate = this.props.inoutsToDate;

        for (var i = 0, total = inoutsToDate.length; i < total; i++){
            var item = inoutsToDate[i];
            if (item.freq == 'o'){
                if (item.amount < 0){
                    totalExpense += item.amount;
                } else {
                    totalIncome += item.amount;
                }
            } else if (item.freq == 'm'){
                // suppose it has right start date,
                // maybe we should set CheckPoint?
                var m = (Helper.getDateDiff(new Date(item.startDate), endDay)).m;
                if (item.amount < 0){
                    totalExpense += item.amount * m;
                } else {
                    totalIncome += item.amount * m;
                }
            }
        }

        var disposableEveryDay = (totalIncome + totalExpense - goal.amount) / rest.d;

        return (
            React.createElement("div", {className: "goal-banner state-" + this.state.state, onClick: this.jumpState}, 
                React.createElement("p", {className: "goal-banner__header show-at-state-0"}, 
                   React.createElement("span", {className: "rest-date"}, rest.d, " days"), 
                   React.createElement("span", {className: "title"}, "GOAL ", goal.amount, " @ ", Helper.formatDate(new Date(goal.date), 'YYYY-MM-DD'))
                ), 
                React.createElement("p", {className: "goal-banner__header show-at-state-1"}, 
                   React.createElement("span", {className: "edit", onClick: this.editGoal}, "EDIT"), 
                   React.createElement("span", {className: "title"}, "@ ", Helper.formatDate(new Date(goal.date), 'YYYY-MM-DD'))
                ), 

                React.createElement("div", {className: "goal-banner__body show-at-state-1"}, 
                    React.createElement("div", {className: "block block-goal"}, 
                        React.createElement("div", null, 
                        React.createElement("div", {className: "num"}, goal.amount), 
                        React.createElement("div", {className: "label"}, "goal")
                        )
                    ), 

                    React.createElement("div", {className: "block block-mark"}, 
                        React.createElement("p", {className: "mark mark-equal"}, "=")
                    ), 

                    React.createElement("div", {className: "block block-income"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, totalIncome), 
                        React.createElement("p", {className: "label"}, "income")
                        )
                    ), 

                    React.createElement("div", {className: "block block-mark"}, 
                        React.createElement("p", {className: "mark mark-equal"}, "-")
                    ), 

                    React.createElement("div", {className: "block block-expense"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, -totalExpense.toFixed(4)), 
                        React.createElement("p", {className: "label"}, "expense")
                        )
                    ), 

                    React.createElement("div", {className: "block block-mark"}, 
                        React.createElement("p", {className: "mark mark-minus"}, "-")
                    ), 

                    React.createElement("div", {className: "block block-today"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, disposableEveryDay.toFixed(3)), 
                        React.createElement("p", {className: "label"}, "quota today")
                        )
                    )
                ), 

                React.createElement("div", {className: "goal-banner__body show-at-state-0"}, 
                    React.createElement("div", {className: "block block-today block-today-center"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, disposableEveryDay.toFixed(3)), 
                        React.createElement("p", {className: "label"}, "you can spend")
                        )
                    )
                )
            )
        )
    }
});


// datepick offser 2 modes
// picker for inout, picker for date
// since inout date maybe something like "every month 25th" 
// so value should be string


// props init
// value: @string

var DatePicker = React.createClass({displayName: "DatePicker",
    mixins: [mixinZLPopup],
    reset: function(){
    },

    getInitialState: function(){

        // value could be some thing not date string.
        // 2015/5/32
        // 2015/every/25
        var year, month, date;
        console.log('init datepicker', this.props.value);
        if (this.props.value){
            var segs = this.props.value.split('/');
            year = segs[0] * 1;
            month = isNaN(segs[1]) ? segs[1] : segs[1] * 1;
            date = isNaN(segs[2]) ? segs[2] : segs[2] * 1;
        } else {
            var initValue = new Date();
            year = initValue.getFullYear();
            month = initValue.getMonth() + 1;
            date = initValue.getDate();
        }

        console.log(year, month, date);

        return {
            year: year,
            month: month,
            date: date,
            state: 2,
            mode: this.props.mode || 'standard'
        }
    },

    setYear: function(year){
        this.setState({
            year: year
        })

        this.jumpState();
    },

    setMonth: function(month){
        console.log(month);
        this.setState({
            month: month
        })
        console.log(this.state);
        this.jumpState();
    },

    setDate: function(date){
        this.state.date = date;
        this.setState(this.state);
        this.confirm();
    },

    jumpState: function(state){
        var state = state != undefined ? state : (this.state.state + 1) % 3;
        this.setState({state: state});
    },

    confirm: function(){
        var value = this.state.year + '/' + this.state.month + '/' + this.state.date;
        $(window).trigger('input.done', {
            value: value
        });
        this.hide();
    },

    cancel: function(){
        $(window).trigger('input.done', {
            value: null
        });
        this.hide();
    },

    render: function(){
        var keysYear = [],
            keysMonth = [],
            keysDate = [],
            mode = this.state.mode;

        if (mode == 'inout'){
            keysYear.push(
                React.createElement("div", {className: "key-year key-every " + ('every' == this.state.year ? "selected" : ""), onClick: this.setYear.bind(this, 'every'), value: "every"}, "Every Year")
            )

            keysYear.push(
                React.createElement("div", {className: "key-year key-some " + ('some' == this.state.year ? "selected" : ""), onClick: this.setYear.bind(this, 'some'), value: "some"}, "Some Year")
            )
        }
        for(var i = 0; i < 10; i++){
            keysYear.push(
                React.createElement("div", {className: "key-year " + (2015 - i == this.state.year ? "selected" : ""), onClick: this.setYear.bind(this, 2015 - i), value: 2015-i}, 2015-i)
                )
        }

        if (mode == 'inout'){
            keysMonth.push(
                React.createElement("div", {className: "key-month key-every " + ('every' == this.state.month ? "selected" : ""), onClick: this.setMonth.bind(this, 'every'), value: "every"}, "Every Month")
            )
            keysMonth.push(
                React.createElement("div", {className: "key-month key-some " + ('some' == this.state.month ? "selected" : ""), onClick: this.setMonth.bind(this, 'some'), value: "some"}, "Some Month")
            )
        }

        for(var i = 1; i < 13; i++){
            keysMonth.push(
                React.createElement("div", {className: "key-month " + (i == this.state.month ? "selected" : ""), onClick: this.setMonth.bind(this,i)}, " ", Helper.getMonthString(i))
                )
        }

        if (mode == 'inout'){
            keysDate.push(
                React.createElement("div", {className: "key-date key-every" + ('every' == this.state.date ? "selected" : ""), onClick: this.setDate.bind(this, 'every'), value: "every"}, "Every Day")
            )
            keysDate.push(
                React.createElement("div", {className: "key-date key-some" + ('some' == this.state.date ? "selected" : ""), onClick: this.setDate .bind(this, 'some'), value: "some"}, "Some day")
            )
        }
        
        for(var i = 1; i < 32; i++){
            keysDate.push(
                React.createElement("div", {className: "key-date " + (i == this.state.date ? "selected" : ""), onClick: this.setDate.bind(this,i)}, Helper.getDateString(i))
                )
        }

        return React.createElement("div", {className: "zl-datepicker state-" + this.state.state}, 
                React.createElement("div", {className: "value"}, 
                    React.createElement("div", {className: "year", onClick: this.jumpState.bind(this,0)}, this.state.year), 
                    React.createElement("div", {className: "month", onClick: this.jumpState.bind(this,1)}, Helper.getMonthString(this.state.month)), 
                    React.createElement("div", {className: "date", onClick: this.jumpState.bind(this,2)}, Helper.getDateString(this.state.date))
                ), 

                React.createElement("div", {className: "pannel"}, 
                    React.createElement("div", {className: "pannel-year show-at-state-0"}, 
                        keysYear
                    ), 

                    React.createElement("div", {className: "pannel-month show-at-state-1"}, 
                        keysMonth
                    ), 

                    React.createElement("div", {className: "pannel-date show-at-state-2"}, 
                        keysDate
                    )
                ), 

                React.createElement("div", {className: "controls"}, 
                    React.createElement("button", {onClick: this.confirm, className: "button-confirm"}, "OK"), 
                    React.createElement("button", {onClick: this.cancel, className: "button-cancel"}, "cancel")
                )
            )
    }
});

var Keyboard = React.createClass({displayName: "Keyboard",
    mixins: [mixinZLPopup],
    getInitialState: function(){
        return {
            value: this.props.value || ''
        }
    },
    click: function(letter){
        var value = this.state.value;
        value += letter.toLowerCase();
        this.setState({value: value});
    },

    backspace: function(){
        var value = this.state.value;
        value = value.slice(0, value.length - 1);
        this.setState({value: value});
    },

    confirm: function(){
        var value = this.state.value;
        $(window).trigger('input.done', {
            value: value.toLowerCase()
        });
        this.hide();
    },

    cancel: function(){
        $(window).trigger('input.done', {
            value: null
        });
        this.hide();
    },

    render: function(){
        var that = this;

        var keys = [];
        function renderKeys(keys){
            return keys.split('').map(function(letter){
                if (letter == '\n'){
                    return React.createElement("br", null)
                } else if (letter == ' ') {
                    return React.createElement("div", {className: "key key-space", onTouchStart: that.click.bind(that, letter)}, React.createElement("div", {className: "inner"}, "space"))
                } else if (letter == '<') {
                    return React.createElement("div", {className: "key key-backspace", onTouchStart: that.backspace}, React.createElement("div", {className: "inner"}, "←"))
                } else {
                    return React.createElement("div", {className: "key key-" + letter, onTouchStart: that.click.bind(that, letter)}, React.createElement("div", {className: "inner"}, letter))
                }
            });
        }

        return (
            React.createElement("div", {className: "keyboard"}, 
                React.createElement("div", {className: "value"}, this.state.value), 
                React.createElement("div", {className: "panel"}, 
                    React.createElement("div", {className: "row1"}, renderKeys('0123456789')), 
                    React.createElement("div", {className: "row2"}, renderKeys('QWERTYUIOP')), 
                    React.createElement("div", {className: "row3"}, renderKeys('ASDFGHJKL')), 
                    React.createElement("div", {className: "row4"}, renderKeys('ZXCVBNM')), 
                    React.createElement("div", {className: "row5"}, renderKeys(' .<'))
                ), 
                React.createElement("div", {className: "controls"}, 
                    React.createElement("button", {onClick: this.confirm, className: "button-confirm"}, "OK"), 
                    React.createElement("button", {onClick: this.cancel, className: "button-cancel"}, "cancel")
                )
            )
        )
    }
});


var Numpad = React.createClass({displayName: "Numpad",

    render: function(){
        return (
            React.createElement("div", {className: "numpad"}, 
                React.createElement("div", {value: "1", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-1"}, "1"), 
                React.createElement("div", {value: "2", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-2"}, "2"), 
                React.createElement("div", {value: "3", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-3"}, "3"), 
                React.createElement("div", {value: "4", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-4"}, "4"), 
                React.createElement("div", {value: "5", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-5"}, "5"), 
                React.createElement("div", {value: "6", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-6"}, "6"), 
                React.createElement("div", {value: "7", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-7"}, "7"), 
                React.createElement("div", {value: "8", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-8"}, "8"), 
                React.createElement("div", {value: "9", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-9"}, "9"), 
                React.createElement("div", {value: "0", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-0"}, "0"), 
                React.createElement("div", {value: ".", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-dot"}, "."), 
                React.createElement("div", {onClick: this.props.backspace, className: "numpad-key numpad-key-backspace"})
            )
        )
    }
});


var ZLInput = React.createClass({displayName: "ZLInput",
    getInitialState: function(){
        var value = null;

        if (this.props.type === 'inout-date'){
            // if inout-date, then value should be string
            if (this.props.value){
                value = this.props.value;
            } else {
                value = Helper.formatDate(new Date(), 'YYYY/MM/DD');
            }
        } else if (this.props.type === 'date'){
            if (this.props.value){
                // TODO
                if (typeof this.props.value === "number"){
                    value = Helper.formatDate(new Date(this.props.value), 'YYYY/MM/DD');
                } else {
                    value = undefined;
                }
            } else {
                value = undefined;
            }
        } else if (this.props.type === 'text'){
            value = this.props.value || '';
        }

        return {
            value: value
        }
    },

    focus: function(){
        var that = this,
            $commonPopup = $('.zl-common-popup'),
            type = this.props.type;

        if (type === 'date'){
            React.render(
                React.createElement(DatePicker, {value: this.state.value, mode: "standard"}),
                $commonPopup[0]
            )
        } else if (type === 'inout-date'){
            React.render(
                React.createElement(DatePicker, {value: this.state.value, mode: "inout"}),
                $commonPopup[0]
            )
        }else if (type === 'text'){
            React.render(
                React.createElement(Keyboard, {value: this.state.value}),
                $commonPopup[0]
            )
        }

        $(window).one('input.done', function(e, data){
            console.log('input value got:', data.value);

            if (that.props.onChange){
                that.props.onChange(data.value);
            }
            if (data.value){
                that.setState({value:data.value});
            }
        })
    },
    render: function(){
        if (this.props.type == 'inout-date'){
            return React.createElement("div", React.__spread({className: "zl-input", onClick: this.focus},  this.props), 
                this.state.value ? (this.props.label + Helper.getValueLabel(this.state.value, this.props.type)) : this.props.placeholder
                )
            
        } else if (this.props.type == 'date'){
            return React.createElement("div", React.__spread({className: "zl-input", onClick: this.focus},  this.props), 
                this.state.value ? (this.props.label + Helper.getValueLabel(this.state.value, this.props.type)) : this.props.placeholder
                )
        } else if (this.props.type == 'text'){
            return React.createElement("div", React.__spread({className: "zl-input", onClick: this.focus},  this.props), 
                    this.state.value || this.props.placeholder
                )
        }
    }
});

// homepage
var Home = React.createClass({displayName: "Home",
    resetData: function(){
        $(window).trigger('data.reset');
    },

    filteredInouts: function(endStamp, startStamp){
        var inouts = this.props.data.inouts;

        // suppose only monthly base datestamp is ok
        // 2015/5/6
        // 2015/every/25
        var t = new Date(startStamp),
            dateStart = t.getDate();

        var tEnd = new Date(endStamp),
            dateEnd = t.getDate();

        if (startStamp){
            return inouts.filter(function(el){
                if (el.dateStamp){
                    return el.dateStamp && el.dateStamp >= startStamp && el.dateStamp < endStamp;
                } else if(el.freq == 'm'){
                    // suppose it has date
                    // its date should be after date
                    var segs = el.date.split('/'),
                        dateInout = segs[2] * 1;

                    return dateInout >= dateStart && dateInout <= dateEnd;
                }
                return false;
            });
        } else {
            return inouts.filter(function(el){
                // [TODO] have to consider the start point maybe
                // this is for balance calculating
                if (el.dateStamp){
                    return el.dateStamp && el.dateStamp < endStamp
                } else {
                    return true;
                }
                return true;
            });
        }
    },

    componentDidMount: function(){

    },

    render: function(){
        var data = this.props.data,
            inoutsOfDate = [],
            inoutsToDate = [];

        var date = Helper.formatDate(this.props.display.date, 'YYYY/MM/DD') + ' 00:00';
        var dateStamp = new Date(date).getTime();

        inoutsOfDate = this.filteredInouts(dateStamp + 24 * 3600 * 1000, dateStamp);
        inoutsToDate = this.filteredInouts(dateStamp + 24 * 3600 * 1000);
        var goal = data.goals ? data.goals[0] : null;

        return (
            React.createElement("div", {className: "home"}, 
                React.createElement(DateHeader, {date: this.props.display.date}), 
                React.createElement(GoalBanner, {inoutsToDate: inoutsToDate, goal: goal, date: this.props.display.date}), 
                React.createElement(InoutList, {data: inoutsOfDate}), 
                React.createElement("div", {className: "bottom-bar"}, 
                    React.createElement(TriggerAddInout, {type: "in"}), React.createElement(TriggerAddInout, {type: "out"})
                )
            )
        );
    }
});

var TriggerAddInout = React.createClass({displayName: "TriggerAddInout",
    mixins: [TriggerPopup],
    popupName: 'inout-editor',
    componentDidMount: function(){
        this.popupExtra = {
            amount: this.props.type == 'in' ? '+' : '-'
        }
    },
    render: function(){
        var className = 'button-add-inout button-add-inout-' + this.props.type;

        return (
            React.createElement("button", React.__spread({onClick: this.popup},  this.props, {className: className}), this.props.children)
        )
    }
});


var InoutForm = React.createClass({displayName: "InoutForm",

    addNewInout: function(){
        var amount = React.findDOMNode(this.refs.amount).value.trim();
        this.props.onAddNewInout(amount * 1);
    },
    render: function(){
        return (
            React.createElement("div", null, 
                React.createElement("input", {type: "text", ref: "amount"}), 
                React.createElement("button", {onClick: this.addNewInout}, "Add")
            )
        )
    }
});

var InoutItem = React.createClass({displayName: "InoutItem",
    getInitialState: function(){
        return {
            state: 0
        }
    },
    jumpState: function(){
        var state = (this.state.state + 1) % 2;
        this.setState({state: state});
    },
    remove: function(){
        $(window).trigger('inout.remove', this.props.data);
    },
    edit: function(){
        var inout = this.props.data;
        $(window).trigger('popup.pop', {popupName: 'inout-editor', popupExtra:{inout:inout}});
    },
    render: function(){
        var item = this.props.data;
        if (!item){
        	return React.createElement("div", {className: "inout-item"}, "no in&out record")
        }
        return (
            React.createElement("div", {className: "inout-item state-" + this.state.state, onClick: this.jumpState}, 
                React.createElement("button", {onClick: this.edit, className: "inout-item__edit"}, "EDIT"), 
                item.amount > 0 ? '+' : '', item.amount, 
                React.createElement("button", {onClick: this.remove, className: "inout-item__remove"}, "DELETE")
            )
        );
    }
});

var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
var InoutList = React.createClass({displayName: "InoutList",
    componentDidMount: function(){
        var $dom = React.findDOMNode(this);
    },
    componentDidUpdate: function(){
        var $dom = React.findDOMNode(this);
            height = window.innerHeight - 123 - 70;

        $($dom).height(height);
        Helper.disableTouchmove(false);
        $(window).off('touchstart.disable');

        var touchY = 0,
            y = 0,
            scrollTop = 0;
        if (height < $dom.scrollHeight){
            $(window).on('touchstart.disable', function(e){
                touchY = e.originalEvent.pageY;
                if ($(e.target).parents().filter('.inout-list').size() > 0){
                    $(window).on('touchmove.disable', function(e){
                        y = e.originalEvent.pageY;
                        scrollTop = $($dom).scrollTop();

                        if ($($dom).scrollTop() == 0 && y > touchY){
                            e.preventDefault();
                        } else if (scrollTop + height >= $dom.scrollHeight && y < touchY){
                            e.preventDefault();
                        }
                    });

                    $(window).one('touchend', function(){
                        Helper.disableTouchmove(false);
                    });
                } else {
                    Helper.disableTouchmove('disable');
                }
            })
        } else {
            Helper.disableTouchmove('disable');
        }
        
    },
    render: function(){
        
        return (
            React.createElement("div", {className: "inout-list needslick"}, 
                React.createElement("div", null, 
                React.createElement(ReactCSSTransitionGroup, {transitionName: "list-item"}, 
                this.props.data.map(function(inout, i){
                    return React.createElement(InoutItem, {data: inout, key: inout.created_at})
                })
                ))
            )
        )
    }
});

// Overlay
var Overlay = React.createClass({displayName: "Overlay",
    getInitialState: function(){
        return {
            isShowing: false
        }
    },

    render: function(){
        return (React.createElement("br", null));
    }
});

var Popup = React.createClass({displayName: "Popup",
    killClick: function(e) {
        // clicks on the content shouldn't close the modal
        e.stopPropagation();
    },
    handleBackdropClick: function() {
        // when you click the background, the user is requesting that the modal gets closed.
        // note that the modal has no say over whether it actually gets closed. the owner of the
        // modal owns the state. this just "asks" to be closed.
        this.props.onRequestClose();
    },
    render: function() {
        return this.transferPropsTo(
            React.createElement("div", {className: "ModalBackdrop", onClick: this.handleBackdropClick}, 
                React.createElement("div", {className: "ModalContent", onClick: this.killClick}, 
                    this.props.children
                )
            )
        );
    }
});

var PopupAddInout = React.createClass({displayName: "PopupAddInout",
    mixins: [popupComponentMixin],
    getInitialState: function(){
        return this.props.inout || {amount: this.props.amount || ''}
    },

    confirm: function(){
        var amount = this.state.amount,
            memo = this.refs.memo.state.value,
            date = this.refs.inputDate.state.value,
            dateStamp = new Date(date).getTime();

        // if date is not valide, new Date() is not invalid.
        // but getTime will return NaN

        var newInout = {
            amount: amount * 1,
            date: date,
            freq: 'o',
            created_at: this.state.created_at || Date.now(),
            is_new: !this.state.created_at,
            memo: memo
        }

        if (dateStamp){
            newInout.dateStamp = dateStamp;
        }

        if (this.refs.startDate){
            newInout.startDate = this.refs.startDate.state.value;
        }

        if (this.refs.endDate){
            newInout.endDate = this.refs.endDate.state.value;
        }

        $(window).trigger('PopupAddInout.done', newInout);
    },

    onClickHandler: function(obj, value){
        var amount = '' + this.state.amount + $(obj.nativeEvent.target).text();
        this.setState({
            amount: amount
        })
    },

    backspace: function(){
        var amount = '' + this.state.amount;
        this.setState({
            amount: amount.slice(0,amount.length - 1)
        });
    },

    updateDate: function(date){
        console.log('PopupAddInout', date);
        this.setState({date: date});
    },

    render: function(){
        var self = this;
        var date = this.state.date,
            isEveryMonth = date && date.split('/')[1] == 'every';

        console.log('isEveryMonth', isEveryMonth);
        var period = false;
        if (isEveryMonth){
            period = React.createElement("div", null, 
                React.createElement(ZLInput, {type: "date", ref: "startDate", placeholder: "start from:", value: this.state.startDate, label: "start from "}), 
                React.createElement(ZLInput, {type: "date", ref: "endDate", placeholder: "end at", value: this.state.endDate, label: "end @ "})
                )
        }
        return (
            React.createElement("div", {className: "popup-addinout"}, 
            React.createElement("div", {className: "popup-addinout__input"}, 
            React.createElement("div", {className: "label"}, "¥"), 
            React.createElement("span", {className: "num"}, this.state.amount)
            ), 
            React.createElement(Numpad, {backspace: this.backspace, onClickHandler: this.onClickHandler}), 
            React.createElement(ZLInput, {type: "inout-date", ref: "inputDate", placeholder: "select date", label: "@", value: this.state.date, onChange: this.updateDate}), 
            period, 
            React.createElement(ZLInput, {type: "text", ref: "memo", placeholder: "memo...", value: this.state.memo}), 
            React.createElement("div", {className: "popup-addinout__actions"}, 
                React.createElement("button", {onClick: this.confirm, className: "button-confirm"}, "OK"), 
                React.createElement("button", {onClick: this.hide, className: "button-cancel"}, "cancel")
            )
            )
        )
    }
});

var PopupCalender = React.createClass({displayName: "PopupCalender",
    mixins: [popupComponentMixin],
    componentDidMount: function(){
        var that = this;
        $(React.findDOMNode(this.refs.datePicker)).datepicker()
            .on('changeDate', function(e){
                that.hide();
                $(window).trigger('datechanged', e.date);
            })
    },
    render: function(){
        return React.createElement("div", {className: "popup-datepicker"}, 
                React.createElement("div", {className: "date-picker", ref: "datePicker"}), 
                React.createElement("div", {className: "controls"}, 
                    React.createElement("button", {onClick: this.confirm, className: "button-confirm"}, "OK"), 
                    React.createElement("button", {onClick: this.hide, className: "button-cancel"}, "cancel")
                )
            )
    }
});

var PopupGoalEditor = React.createClass({displayName: "PopupGoalEditor",
    mixins: [popupComponentMixin],
    getInitialState: function(){
        console.log('init', this.props.goal);
        return this.props.goal|| {amount:''};
    },

    confirm: function(){
        var amount = this.state.amount,
            date = this.refs.inputDate.state.value;

        $(window).trigger('PopupGoalEditor.done', {
            amount: amount * 1,
            date: new Date(date).getTime(),
            created_at: Date.now(),
        });
    },

    onClickHandler: function(obj, value){
        var amount = this.state.amount + $(obj.nativeEvent.target).text();
        this.setState({
            amount: amount
        })
    },

    backspace: function(){
        var amount = '' + this.state.amount;
        this.setState({
            amount: amount.slice(0,amount.length - 1)
        });
    },
    render: function(){
        var self = this;
        console.log('render', this.state.amount);
        return (
            React.createElement("div", {className: "popup-goal-editor"}, 
            React.createElement("h1", {className: "title"}, "edit the goal"), 
            React.createElement("div", {className: "input-amount"}, 
            React.createElement("div", {className: "label"}, "¥"), 
            React.createElement("span", {className: "num"}, this.state.amount)
            ), 
            React.createElement(Numpad, {backspace: this.backspace, onClickHandler: this.onClickHandler}), 
            React.createElement(ZLInput, {type: "date", ref: "inputDate", placeholder: "select date", value: this.state.date, label: "before "}), 
            React.createElement("div", {className: "actions"}, 
                React.createElement("button", {onClick: this.confirm, className: "button-confirm"}, "OK"), 
                React.createElement("button", {onClick: this.hide, className: "button-cancel"}, "cancel")
            )
            )
        )
    }
});

var PopupTrigger = React.createClass({displayName: "PopupTrigger",
    handleClick: function() {
        var popupName = this.props.popupName;
        alert(popupName);
    },
    render: function() {
        return React.createElement("button", {onClick: this.handleClick()}, this.props.text);
    }
});

// homepage
var App = React.createClass({displayName: "App",
    getInitialState: function(){
        var that = this;
        Data.get().then(function(store){
            that.setState({data: store})
        });

        return {
            data: {
                inouts: [],
                balances: [],
                goals: []
            },
            route: '/home',
            popup: false,
            isShowingPopup: false,
            // popup: 'inout-editor',
            // isShowingPopup: true,

            display: {
                date: new Date(),
            }
        }
    },

    componentDidMount: function(){
        var that = this;
        $(window).on('popup.pop', function(e,data){
            console.log('pop',data);
            that.setState({popup: data.popupName, popupExtra: data.popupExtra,isShowingPopup: true})
        });

        $(window).on('PopupAddInout.done', function(e, data){
            var inouts = that.state.data.inouts,
                is_new = data.is_new;

            delete data.is_new;

            // 2015/5/24 => May 24, 2015 => o
            // 2015/5/every => everyday @ May, 2015 => d
            // 2015/5/some => May, 2015 => o

            // 2015/every/24 => 24th @every month, 2015 => m
            // 2015/every/every => every day, 2015 => d
            // 2015/every/some => every month, 2015 => m

            // 2015/some/24 => X
            // 2015/some/every => X
            // 2015/some/some => some time @2015 =>o
            data.freq = Helper.getFreqOfDate(data.date);
            console.log(data);
            if (is_new){
                var newInout = $.extend({}, data);
                inouts.push(newInout);
            } else {
                var updated = false;
                for(var i = 0, total = inouts.length; i < total; i++){
                    if (inouts[i].created_at == data.created_at){
                        $.extend(inouts[i], data);
                        updated = true;
                        break;
                    }
                }
                if (!updated){alert('not updated, inout item not found')}
            }

            that.setState({
                data: that.state.data,
                isShowingPopup: false
            })

            Data.sync();
        });

        $(window).on('PopupGoalEditor.done', function(e, data){
            console.log(data);
            var goal = that.state.data.goals.length ? that.state.data.goals[0] : {};

            $.extend(goal, data);

            that.setState({
                data:that.state.data,
                isShowingPopup: false
            })

            Data.sync();
        });

        $(window).on('inout.remove', function(e,data){
            var inouts = that.state.data.inouts,
                item = data;

            inouts.splice(inouts.indexOf(item), 1);
            that.setState({
                data: that.state.data
            })

            Data.sync();
        })

        $(window).on('showprevday', function(){
            var date = that.state.display.date;
            var newDate = new Date(date.getTime() - 24 * 3600 * 1000);
            that.state.display.date = newDate;
            that.setState({display: that.state.display});
        });

        $(window).on('shownextday', function(){
            var date = that.state.display.date;
            var newDate = new Date(date.getTime() + 24 * 3600 * 1000);
            that.state.display.date = newDate;
            that.setState({display: that.state.display});
        });

        $(window).on('popup.hide', function(e, data){
            that.hidePopup();
        });

        $(window).on('datechanged', function(e,date){
            console.log('datechanged', date);
            var display = that.state.display;
            display.date = date;
            that.setState({display: display});
        });

        $(window).on('data.reset', function(e,data){
            Data.mock();
            location.reload();
        });

    },
    hidePopup: function(){
        this.setState({isShowingPopup: false, popup: null});
    },

    render: function(){
        var page = null,
            popup = null;

        if (this.state.route === '/home'){
            page = React.createElement(Home, {data: this.state.data, display: this.state.display});
        } else {
            page = React.createElement("p", null, "404");
        }

        if (this.state.isShowingPopup){
            var popupContent = null;
            if (this.state.popup === 'inout-editor'){
                popupContent = React.createElement(PopupAddInout, React.__spread({},  this.state.popupExtra));
            } else if (this.state.popup === 'popup-datepicker'){
                popupContent = React.createElement(PopupCalender, React.__spread({},  this.state.popupExtra));
            } else if (this.state.popup === 'goal-editor'){
                popupContent = React.createElement(PopupGoalEditor, React.__spread({},  this.state.popupExtra))
            }

            popup = React.createElement("div", {className: "popup"}, 
                popupContent
                )
        }

        return (
            React.createElement("div", {className: "wrapper"}, 
                page, 
                popup, 
                React.createElement("div", {className: "zl-common-popup"})
            )
        );
    }
});

React.initializeTouchEvents(true);

React.render(
    React.createElement(App, null),
    $('#view')[0]
)

// React.render(
//     <Keyboard />,
//     $('.zl-common-popup')[0]
// )

$(window).on('touchmove', function(e){
    console.log(e.originalEvent.pageY);
});

