module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        react: {
            dynamic_mappings: {
                files: [
                    {
                      expand: true,
                      cwd: 'jsx/',
                      src: ['**/*.js'],
                      dest: '.tmp/js/react/',
                      ext: '.js'
                    }
                ]
            }
        },
        connect: {
            server: {
                options: {
                    port: 9000,
                    base: './'
                }
            }
        },
        concat: {
           jsx: {
                src: [
                    'js/util/helper.js',
                    'js/service/data.js',
                    '.tmp/js/react/mixin/**/*.js',
                    '.tmp/js/react/components/**/*.js',
                    '.tmp/js/react/index.js'
                    ],
                dest: 'dist/js/app.js',
           },
           vender: {
                src: [
                    'js/lib/jquery-1.10.0.min.js',
                    'js/lib/react-with-addons.js',
                    'js/lib/fastclick.js',
                    'js/lib/bootstrap-datepicker.js'
                ],
                dest: 'dist/js/vendor.js',
           }
        },
        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'css/main.css': 'sass/main.sass'
                }
            }
        },
        watch: {
            sass: {
                files: ['**/*.sass','index.html'],
                tasks: ['sass'],
            },

            jsx: {
                files: ['./jsx/**/*.js'],
                tasks: ['react', 'concat']
            },

            js: {
                files: ['js/**/*.js'],
                tasks: ['concat'],
            },
            options: {
                livereload: true
            }
        }
    });

    grunt.registerTask('default', ['connect', 'watch']);
};
