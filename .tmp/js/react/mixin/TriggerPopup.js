var TriggerPopup = {
    popup: function(){
        var popup = this.popupName,
        	extra = this.popupExtra;

        $(window).trigger('popup.pop', {popupName: popup, popupExtra: extra});
    }
};
