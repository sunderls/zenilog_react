var PopupTrigger = React.createClass({displayName: "PopupTrigger",
    handleClick: function() {
        var popupName = this.props.popupName;
        alert(popupName);
    },
    render: function() {
        return React.createElement("button", {onClick: this.handleClick()}, this.props.text);
    }
});
