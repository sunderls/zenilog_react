var Numpad = React.createClass({displayName: "Numpad",

    render: function(){
        return (
            React.createElement("div", {className: "numpad"}, 
                React.createElement("div", {value: "1", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-1"}, "1"), 
                React.createElement("div", {value: "2", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-2"}, "2"), 
                React.createElement("div", {value: "3", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-3"}, "3"), 
                React.createElement("div", {value: "4", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-4"}, "4"), 
                React.createElement("div", {value: "5", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-5"}, "5"), 
                React.createElement("div", {value: "6", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-6"}, "6"), 
                React.createElement("div", {value: "7", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-7"}, "7"), 
                React.createElement("div", {value: "8", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-8"}, "8"), 
                React.createElement("div", {value: "9", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-9"}, "9"), 
                React.createElement("div", {value: "0", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-0"}, "0"), 
                React.createElement("div", {value: ".", onClick: this.props.onClickHandler, className: "numpad-key numpad-key-dot"}, "."), 
                React.createElement("div", {onClick: this.props.backspace, className: "numpad-key numpad-key-backspace"})
            )
        )
    }
});

