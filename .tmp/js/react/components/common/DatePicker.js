// datepick offser 2 modes
// picker for inout, picker for date
// since inout date maybe something like "every month 25th" 
// so value should be string


// props init
// value: @string

var DatePicker = React.createClass({displayName: "DatePicker",
    mixins: [mixinZLPopup],
    reset: function(){
    },

    getInitialState: function(){

        // value could be some thing not date string.
        // 2015/5/32
        // 2015/every/25
        var year, month, date;
        console.log('init datepicker', this.props.value);
        if (this.props.value){
            var segs = this.props.value.split('/');
            year = segs[0] * 1;
            month = isNaN(segs[1]) ? segs[1] : segs[1] * 1;
            date = isNaN(segs[2]) ? segs[2] : segs[2] * 1;
        } else {
            var initValue = new Date();
            year = initValue.getFullYear();
            month = initValue.getMonth() + 1;
            date = initValue.getDate();
        }

        console.log(year, month, date);

        return {
            year: year,
            month: month,
            date: date,
            state: 2,
            mode: this.props.mode || 'standard'
        }
    },

    setYear: function(year){
        this.setState({
            year: year
        })

        this.jumpState();
    },

    setMonth: function(month){
        console.log(month);
        this.setState({
            month: month
        })
        console.log(this.state);
        this.jumpState();
    },

    setDate: function(date){
        this.state.date = date;
        this.setState(this.state);
        this.confirm();
    },

    jumpState: function(state){
        var state = state != undefined ? state : (this.state.state + 1) % 3;
        this.setState({state: state});
    },

    confirm: function(){
        var value = this.state.year + '/' + this.state.month + '/' + this.state.date;
        $(window).trigger('input.done', {
            value: value
        });
        this.hide();
    },

    cancel: function(){
        $(window).trigger('input.done', {
            value: null
        });
        this.hide();
    },

    render: function(){
        var keysYear = [],
            keysMonth = [],
            keysDate = [],
            mode = this.state.mode;

        if (mode == 'inout'){
            keysYear.push(
                React.createElement("div", {className: "key-year key-every " + ('every' == this.state.year ? "selected" : ""), onClick: this.setYear.bind(this, 'every'), value: "every"}, "Every Year")
            )

            keysYear.push(
                React.createElement("div", {className: "key-year key-some " + ('some' == this.state.year ? "selected" : ""), onClick: this.setYear.bind(this, 'some'), value: "some"}, "Some Year")
            )
        }
        for(var i = 0; i < 10; i++){
            keysYear.push(
                React.createElement("div", {className: "key-year " + (2015 - i == this.state.year ? "selected" : ""), onClick: this.setYear.bind(this, 2015 - i), value: 2015-i}, 2015-i)
                )
        }

        if (mode == 'inout'){
            keysMonth.push(
                React.createElement("div", {className: "key-month key-every " + ('every' == this.state.month ? "selected" : ""), onClick: this.setMonth.bind(this, 'every'), value: "every"}, "Every Month")
            )
            keysMonth.push(
                React.createElement("div", {className: "key-month key-some " + ('some' == this.state.month ? "selected" : ""), onClick: this.setMonth.bind(this, 'some'), value: "some"}, "Some Month")
            )
        }

        for(var i = 1; i < 13; i++){
            keysMonth.push(
                React.createElement("div", {className: "key-month " + (i == this.state.month ? "selected" : ""), onClick: this.setMonth.bind(this,i)}, " ", Helper.getMonthString(i))
                )
        }

        if (mode == 'inout'){
            keysDate.push(
                React.createElement("div", {className: "key-date key-every" + ('every' == this.state.date ? "selected" : ""), onClick: this.setDate.bind(this, 'every'), value: "every"}, "Every Day")
            )
            keysDate.push(
                React.createElement("div", {className: "key-date key-some" + ('some' == this.state.date ? "selected" : ""), onClick: this.setDate .bind(this, 'some'), value: "some"}, "Some day")
            )
        }
        
        for(var i = 1; i < 32; i++){
            keysDate.push(
                React.createElement("div", {className: "key-date " + (i == this.state.date ? "selected" : ""), onClick: this.setDate.bind(this,i)}, Helper.getDateString(i))
                )
        }

        return React.createElement("div", {className: "zl-datepicker state-" + this.state.state}, 
                React.createElement("div", {className: "value"}, 
                    React.createElement("div", {className: "year", onClick: this.jumpState.bind(this,0)}, this.state.year), 
                    React.createElement("div", {className: "month", onClick: this.jumpState.bind(this,1)}, Helper.getMonthString(this.state.month)), 
                    React.createElement("div", {className: "date", onClick: this.jumpState.bind(this,2)}, Helper.getDateString(this.state.date))
                ), 

                React.createElement("div", {className: "pannel"}, 
                    React.createElement("div", {className: "pannel-year show-at-state-0"}, 
                        keysYear
                    ), 

                    React.createElement("div", {className: "pannel-month show-at-state-1"}, 
                        keysMonth
                    ), 

                    React.createElement("div", {className: "pannel-date show-at-state-2"}, 
                        keysDate
                    )
                ), 

                React.createElement("div", {className: "controls"}, 
                    React.createElement("button", {onClick: this.confirm, className: "button-confirm"}, "OK"), 
                    React.createElement("button", {onClick: this.cancel, className: "button-cancel"}, "cancel")
                )
            )
    }
});
