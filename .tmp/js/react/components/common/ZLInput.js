var ZLInput = React.createClass({displayName: "ZLInput",
    getInitialState: function(){
        var value = null;

        if (this.props.type === 'inout-date'){
            // if inout-date, then value should be string
            if (this.props.value){
                value = this.props.value;
            } else {
                value = Helper.formatDate(new Date(), 'YYYY/MM/DD');
            }
        } else if (this.props.type === 'date'){
            if (this.props.value){
                // TODO
                if (typeof this.props.value === "number"){
                    value = Helper.formatDate(new Date(this.props.value), 'YYYY/MM/DD');
                } else {
                    value = undefined;
                }
            } else {
                value = undefined;
            }
        } else if (this.props.type === 'text'){
            value = this.props.value || '';
        }

        return {
            value: value
        }
    },

    focus: function(){
        var that = this,
            $commonPopup = $('.zl-common-popup'),
            type = this.props.type;

        if (type === 'date'){
            React.render(
                React.createElement(DatePicker, {value: this.state.value, mode: "standard"}),
                $commonPopup[0]
            )
        } else if (type === 'inout-date'){
            React.render(
                React.createElement(DatePicker, {value: this.state.value, mode: "inout"}),
                $commonPopup[0]
            )
        }else if (type === 'text'){
            React.render(
                React.createElement(Keyboard, {value: this.state.value}),
                $commonPopup[0]
            )
        }

        $(window).one('input.done', function(e, data){
            console.log('input value got:', data.value);

            if (that.props.onChange){
                that.props.onChange(data.value);
            }
            if (data.value){
                that.setState({value:data.value});
            }
        })
    },
    render: function(){
        if (this.props.type == 'inout-date'){
            return React.createElement("div", React.__spread({className: "zl-input", onClick: this.focus},  this.props), 
                this.state.value ? (this.props.label + Helper.getValueLabel(this.state.value, this.props.type)) : this.props.placeholder
                )
            
        } else if (this.props.type == 'date'){
            return React.createElement("div", React.__spread({className: "zl-input", onClick: this.focus},  this.props), 
                this.state.value ? (this.props.label + Helper.getValueLabel(this.state.value, this.props.type)) : this.props.placeholder
                )
        } else if (this.props.type == 'text'){
            return React.createElement("div", React.__spread({className: "zl-input", onClick: this.focus},  this.props), 
                    this.state.value || this.props.placeholder
                )
        }
    }
});
