var Keyboard = React.createClass({displayName: "Keyboard",
    mixins: [mixinZLPopup],
    getInitialState: function(){
        return {
            value: this.props.value || ''
        }
    },
    click: function(letter){
        var value = this.state.value;
        value += letter.toLowerCase();
        this.setState({value: value});
    },

    backspace: function(){
        var value = this.state.value;
        value = value.slice(0, value.length - 1);
        this.setState({value: value});
    },

    confirm: function(){
        var value = this.state.value;
        $(window).trigger('input.done', {
            value: value.toLowerCase()
        });
        this.hide();
    },

    cancel: function(){
        $(window).trigger('input.done', {
            value: null
        });
        this.hide();
    },

    render: function(){
        var that = this;

        var keys = [];
        function renderKeys(keys){
            return keys.split('').map(function(letter){
                if (letter == '\n'){
                    return React.createElement("br", null)
                } else if (letter == ' ') {
                    return React.createElement("div", {className: "key key-space", onTouchStart: that.click.bind(that, letter)}, React.createElement("div", {className: "inner"}, "space"))
                } else if (letter == '<') {
                    return React.createElement("div", {className: "key key-backspace", onTouchStart: that.backspace}, React.createElement("div", {className: "inner"}, "←"))
                } else {
                    return React.createElement("div", {className: "key key-" + letter, onTouchStart: that.click.bind(that, letter)}, React.createElement("div", {className: "inner"}, letter))
                }
            });
        }

        return (
            React.createElement("div", {className: "keyboard"}, 
                React.createElement("div", {className: "value"}, this.state.value), 
                React.createElement("div", {className: "panel"}, 
                    React.createElement("div", {className: "row1"}, renderKeys('0123456789')), 
                    React.createElement("div", {className: "row2"}, renderKeys('QWERTYUIOP')), 
                    React.createElement("div", {className: "row3"}, renderKeys('ASDFGHJKL')), 
                    React.createElement("div", {className: "row4"}, renderKeys('ZXCVBNM')), 
                    React.createElement("div", {className: "row5"}, renderKeys(' .<'))
                ), 
                React.createElement("div", {className: "controls"}, 
                    React.createElement("button", {onClick: this.confirm, className: "button-confirm"}, "OK"), 
                    React.createElement("button", {onClick: this.cancel, className: "button-cancel"}, "cancel")
                )
            )
        )
    }
});

