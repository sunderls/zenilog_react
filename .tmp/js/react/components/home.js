// homepage
var Home = React.createClass({displayName: "Home",
    resetData: function(){
        $(window).trigger('data.reset');
    },

    filteredInouts: function(endStamp, startStamp){
        var inouts = this.props.data.inouts;

        // suppose only monthly base datestamp is ok
        // 2015/5/6
        // 2015/every/25
        var t = new Date(startStamp),
            dateStart = t.getDate();

        var tEnd = new Date(endStamp),
            dateEnd = t.getDate();

        if (startStamp){
            return inouts.filter(function(el){
                if (el.dateStamp){
                    return el.dateStamp && el.dateStamp >= startStamp && el.dateStamp < endStamp;
                } else if(el.freq == 'm'){
                    // suppose it has date
                    // its date should be after date
                    var segs = el.date.split('/'),
                        dateInout = segs[2] * 1;

                    return dateInout >= dateStart && dateInout <= dateEnd;
                }
                return false;
            });
        } else {
            return inouts.filter(function(el){
                // [TODO] have to consider the start point maybe
                // this is for balance calculating
                if (el.dateStamp){
                    return el.dateStamp && el.dateStamp < endStamp
                } else {
                    return true;
                }
                return true;
            });
        }
    },

    componentDidMount: function(){

    },

    render: function(){
        var data = this.props.data,
            inoutsOfDate = [],
            inoutsToDate = [];

        var date = Helper.formatDate(this.props.display.date, 'YYYY/MM/DD') + ' 00:00';
        var dateStamp = new Date(date).getTime();

        inoutsOfDate = this.filteredInouts(dateStamp + 24 * 3600 * 1000, dateStamp);
        inoutsToDate = this.filteredInouts(dateStamp + 24 * 3600 * 1000);
        var goal = data.goals ? data.goals[0] : null;

        return (
            React.createElement("div", {className: "home"}, 
                React.createElement(DateHeader, {date: this.props.display.date}), 
                React.createElement(GoalBanner, {inoutsToDate: inoutsToDate, goal: goal, date: this.props.display.date}), 
                React.createElement(InoutList, {data: inoutsOfDate}), 
                React.createElement("div", {className: "bottom-bar"}, 
                    React.createElement(TriggerAddInout, {type: "in"}), React.createElement(TriggerAddInout, {type: "out"})
                )
            )
        );
    }
});
