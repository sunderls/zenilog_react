var Popup = React.createClass({displayName: "Popup",
    killClick: function(e) {
        // clicks on the content shouldn't close the modal
        e.stopPropagation();
    },
    handleBackdropClick: function() {
        // when you click the background, the user is requesting that the modal gets closed.
        // note that the modal has no say over whether it actually gets closed. the owner of the
        // modal owns the state. this just "asks" to be closed.
        this.props.onRequestClose();
    },
    render: function() {
        return this.transferPropsTo(
            React.createElement("div", {className: "ModalBackdrop", onClick: this.handleBackdropClick}, 
                React.createElement("div", {className: "ModalContent", onClick: this.killClick}, 
                    this.props.children
                )
            )
        );
    }
});
