var PopupCalender = React.createClass({displayName: "PopupCalender",
    mixins: [popupComponentMixin],
    componentDidMount: function(){
        var that = this;
        $(React.findDOMNode(this.refs.datePicker)).datepicker()
            .on('changeDate', function(e){
                that.hide();
                $(window).trigger('datechanged', e.date);
            })
    },
    render: function(){
        return React.createElement("div", {className: "popup-datepicker"}, 
                React.createElement("div", {className: "date-picker", ref: "datePicker"}), 
                React.createElement("div", {className: "controls"}, 
                    React.createElement("button", {onClick: this.confirm, className: "button-confirm"}, "OK"), 
                    React.createElement("button", {onClick: this.hide, className: "button-cancel"}, "cancel")
                )
            )
    }
});
