var InoutItem = React.createClass({displayName: "InoutItem",
    getInitialState: function(){
        return {
            state: 0
        }
    },
    jumpState: function(){
        var state = (this.state.state + 1) % 2;
        this.setState({state: state});
    },
    remove: function(){
        $(window).trigger('inout.remove', this.props.data);
    },
    edit: function(){
        var inout = this.props.data;
        $(window).trigger('popup.pop', {popupName: 'inout-editor', popupExtra:{inout:inout}});
    },
    render: function(){
        var item = this.props.data;
        if (!item){
        	return React.createElement("div", {className: "inout-item"}, "no in&out record")
        }
        return (
            React.createElement("div", {className: "inout-item state-" + this.state.state, onClick: this.jumpState}, 
                React.createElement("button", {onClick: this.edit, className: "inout-item__edit"}, "EDIT"), 
                item.amount > 0 ? '+' : '', item.amount, 
                React.createElement("button", {onClick: this.remove, className: "inout-item__remove"}, "DELETE")
            )
        );
    }
});
