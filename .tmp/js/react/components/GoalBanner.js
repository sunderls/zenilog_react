var GoalBanner = React.createClass({displayName: "GoalBanner",
    getInitialState: function(){
        return {
            state: 0
        }
    },
    jumpState: function(){
        var state = (this.state.state + 1) % 2;
        this.setState({state: state});
    },
    editGoal: function(){
        var goal = this.props.goal;
        $(window).trigger('popup.pop', {popupName: 'goal-editor', popupExtra:{goal:goal}});
    },
    render: function(){

        var goal = this.props.goal,
            date = new Date(this.props.date);

        if (!goal){
            return React.createElement("div", null)
        }
        var endDay = new Date(goal.date);


        var rest = Helper.getDateDiff(date, endDay);

        var totalIncome = 0, totalExpense = 0,
            inoutsToDate = this.props.inoutsToDate;

        for (var i = 0, total = inoutsToDate.length; i < total; i++){
            var item = inoutsToDate[i];
            if (item.freq == 'o'){
                if (item.amount < 0){
                    totalExpense += item.amount;
                } else {
                    totalIncome += item.amount;
                }
            } else if (item.freq == 'm'){
                // suppose it has right start date,
                // maybe we should set CheckPoint?
                var m = (Helper.getDateDiff(new Date(item.startDate), endDay)).m;
                if (item.amount < 0){
                    totalExpense += item.amount * m;
                } else {
                    totalIncome += item.amount * m;
                }
            }
        }

        var disposableEveryDay = (totalIncome + totalExpense - goal.amount) / rest.d;

        return (
            React.createElement("div", {className: "goal-banner state-" + this.state.state, onClick: this.jumpState}, 
                React.createElement("p", {className: "goal-banner__header show-at-state-0"}, 
                   React.createElement("span", {className: "rest-date"}, rest.d, " days"), 
                   React.createElement("span", {className: "title"}, "GOAL ", goal.amount, " @ ", Helper.formatDate(new Date(goal.date), 'YYYY-MM-DD'))
                ), 
                React.createElement("p", {className: "goal-banner__header show-at-state-1"}, 
                   React.createElement("span", {className: "edit", onClick: this.editGoal}, "EDIT"), 
                   React.createElement("span", {className: "title"}, "@ ", Helper.formatDate(new Date(goal.date), 'YYYY-MM-DD'))
                ), 

                React.createElement("div", {className: "goal-banner__body show-at-state-1"}, 
                    React.createElement("div", {className: "block block-goal"}, 
                        React.createElement("div", null, 
                        React.createElement("div", {className: "num"}, goal.amount), 
                        React.createElement("div", {className: "label"}, "goal")
                        )
                    ), 

                    React.createElement("div", {className: "block block-mark"}, 
                        React.createElement("p", {className: "mark mark-equal"}, "=")
                    ), 

                    React.createElement("div", {className: "block block-income"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, totalIncome), 
                        React.createElement("p", {className: "label"}, "income")
                        )
                    ), 

                    React.createElement("div", {className: "block block-mark"}, 
                        React.createElement("p", {className: "mark mark-equal"}, "-")
                    ), 

                    React.createElement("div", {className: "block block-expense"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, -totalExpense.toFixed(4)), 
                        React.createElement("p", {className: "label"}, "expense")
                        )
                    ), 

                    React.createElement("div", {className: "block block-mark"}, 
                        React.createElement("p", {className: "mark mark-minus"}, "-")
                    ), 

                    React.createElement("div", {className: "block block-today"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, disposableEveryDay.toFixed(3)), 
                        React.createElement("p", {className: "label"}, "quota today")
                        )
                    )
                ), 

                React.createElement("div", {className: "goal-banner__body show-at-state-0"}, 
                    React.createElement("div", {className: "block block-today block-today-center"}, 
                        React.createElement("div", null, 
                        React.createElement("p", {className: "num"}, disposableEveryDay.toFixed(3)), 
                        React.createElement("p", {className: "label"}, "you can spend")
                        )
                    )
                )
            )
        )
    }
});

